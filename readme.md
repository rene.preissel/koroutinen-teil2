# Beispiele zum Artikel "Koroutinen in Kotlin: Parallelität" im JavaMagazin

Unter *src/test/kotlin* findet sich eine Testklasse, aus denen die freistehenden Beispiele für
Channels und Aktoren entnommen worden sind.

Unter *main/src/kotlin* finden sich die Datei *CollageFunktionen* in der
alle Koroutinen für die Collage enthalten sind. Zusätzlich noch die Datei *KtorChatServer*,
 die die Websocket-Implementierung enthält.