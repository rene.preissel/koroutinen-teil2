package de.e2.koroutine2

import io.ktor.application.install
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.launch

fun main() {
embeddedServer(Netty, port = 8080) {
    install(WebSockets)

    val broadcastChannel = BroadcastChannel<String>(10)

    routing {
        webSocket("/chat") {
            val receiveChannel = broadcastChannel.openSubscription()
            val broadcastJob = launch {
                for (chatMessage in receiveChannel) {
                    outgoing.send(Frame.Text(chatMessage))
                }
            }

            for (message in incoming) {
                if (message is Frame.Text) {
                    broadcastChannel.send(message.readText())
                }
            }
            broadcastJob.cancel()
            receiveChannel.cancel()
        }
    }
}.start(wait = true)
}