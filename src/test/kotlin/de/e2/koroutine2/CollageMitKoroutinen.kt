package p07_koroutinen

import de.e2.koroutine2.combineImages
import de.e2.koroutine2.createCollageAsyncAwait
import de.e2.koroutine2.requestImageData
import de.e2.koroutine2.requestImageUrls
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.channels.take
import kotlinx.coroutines.channels.toList
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.selects.selectUnbiased
import kotlinx.coroutines.time.delay
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.awt.image.BufferedImage
import java.io.FileOutputStream
import java.time.Duration
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import javax.imageio.ImageIO
import kotlin.concurrent.withLock


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CollageMitKoroutinen {


    @Test
    fun `01a Race Conditions durch Paralellitaet`() = runBlocking {
        suspend fun createCollage(query: String, count: Int) = coroutineScope {
            val urls = requestImageUrls(query, count)
            val images = mutableListOf<BufferedImage>()
            urls.forEach { url ->
                launch {
                    val image = requestImageData(url)
                    images.add(image)
                }
            }
            combineImages(images)
        }


        val image = createCollage("turtle", 10)
        println("${image.width}x${image.height}")
    }

    @Test
    fun `01b Locks zur Vermeidung von Race Conditions`() = runBlocking {
        suspend fun createCollage(query: String, count: Int): BufferedImage {
            val urls = requestImageUrls(query, count)
            val images = mutableListOf<BufferedImage>()
            val imagesLock = ReentrantLock()
            coroutineScope {
                urls.forEach { url ->
                    launch {
                        val image = requestImageData(url)
                        imagesLock.withLock {
                            images.add(image)
                        }
                    }
                }
            }
            return combineImages(images)
        }


        val image = createCollage("turtle", 10)
        println("${image.width}x${image.height}")
    }

    @Test
    fun `01c Collage asynchron mit async await laden`() = runBlocking {
        val image = createCollageAsyncAwait("dogs", 20)
        ImageIO.write(image, "png", FileOutputStream("dogs-n.png"))
        println("${image.width}x${image.height}")
    }

    @Test
    fun `02 Channels Einstieg`() = runBlocking() {
        val channel = Channel<Int>(1)
        launch {
            repeat(5) {
                channel.send(it)
                delay(Duration.ofSeconds(1))
            }

            channel.close()
        }

        for (zahl in channel) {
            println(zahl) //0,1,2,3,4
        }
    }


    @Test
    fun `03 Paralleles Laden mit Channels`() = runBlocking {
        suspend fun createCollage(query: String, count: Int) = coroutineScope {
            val urlChannel = Channel<String>(count)
            val imageChannel = Channel<BufferedImage>(count)

            repeat(5) {
                launch {
                    for (url in urlChannel) {
                        val image = requestImageData(url)
                        imageChannel.send(image)
                    }
                }
            }

            requestImageUrls(query, count).forEach {
                urlChannel.send(it)
            }
            urlChannel.close()

            val images = imageChannel.take(count).toList()
            combineImages(images)
        }

        val image = createCollage("dog", 20)
        println("${image.width}x${image.height}")
    }

    @Test
    fun `04 Aktor und Cache`() = runBlocking<Unit> {

        data class CollageQuery(val query: String, val count: Int) {
            val resultImage: CompletableDeferred<BufferedImage> = CompletableDeferred()
        }

        val collageActor = actor<CollageQuery> {
            val imageCache = WeakHashMap<CollageQuery, BufferedImage>()

            for (collageQuery in channel) {
                val cachedImage = imageCache[collageQuery]
                if (cachedImage != null) {
                    collageQuery.resultImage.complete(cachedImage)
                    continue
                }

                val image = createCollageAsyncAwait(collageQuery.query, collageQuery.count)
                imageCache[collageQuery] = image
                collageQuery.resultImage.complete(image)
            }
        }
        val msg = CollageQuery("dog", 20)
        collageActor.send(msg)
        val image = msg.resultImage.await()
        println("${image.width}x${image.height}")

        val msg2 = CollageQuery("dog", 20)
        collageActor.send(msg2)
        println("${image.width}x${image.height}")

        collageActor.close()
    }


    @Test
    fun `05 Das schnellste Bild Laden mit Producern`() = runBlocking {
        suspend fun createCollage(vararg queries: String, count: Int) = coroutineScope {
            val producerChannels = queries.map { query ->
                produce<BufferedImage>(capacity = count) {
                    val urls = requestImageUrls(query, count)
                    urls.forEach { url ->
                        val image = requestImageData(url)
                        channel.send(image)
                    }
                    close()
                }
            }
            val images = mutableListOf<BufferedImage>()
            while (images.size < count) {
                selectUnbiased<Unit> {
                    producerChannels.forEach { channel ->
                        channel.onReceive { image ->
                            images.add(image)
                        }
                    }
                }
            }
            producerChannels.forEach { it.cancel() }
            combineImages(images)
        }

        val image = createCollage("dog", "cat", count = 20)
        println("${image.width}x${image.height}")
    }
}


